### Pre Requisites

1 - Docker : https://docs.docker.com/engine/install/
2 - Docker compose: https://docs.docker.com/compose/install/
3 - node v16
4 - npm v8
5 - create a `pg-data` folder alongside `src`

### Setup Variables

modify `.env` file

### Run Locally

docker compose up

### Common Errors

1 - pg-data permission denied

- run `sudo chmod -R 777 pg-data`

or give full access to the folder

### Use Locally

http://localhost:3000/docs

Use the tryout swagger function
