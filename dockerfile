FROM node:16-alpine

RUN mkdir -p /users-api
COPY . /users-api
WORKDIR /users-api

RUN npm install

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
RUN chmod +x /wait

CMD /wait && npm start

