import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { UsersDTO } from './dto/users.dto';
import { validate } from 'class-validator';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from '../users/users.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './dto/UserType';

@Injectable()
export class AuthService implements OnModuleInit {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(Users) private usersRepository: Repository<Users>,
  ) {}

  onModuleInit = async () => {
    const defaultUserEmail = 'marcelo@test.com';
    const defaultUser = await this.usersRepository.findOne({
      where: { email: defaultUserEmail },
    });

    // Create the default user if it does not exist
    if (!defaultUser) {
      const userDTO = new UsersDTO();
      userDTO.email = defaultUserEmail;
      userDTO.password = bcrypt.hashSync('defaultuserpass', 10);
      const newUser = await this.usersRepository
        .save(userDTO)
        .catch((error) => {
          this.logger.debug(error.message, AuthService.name);
        });
      console.log('New Default User', newUser);
    } else {
      console.log('Default User', defaultUser);
    }
  };

  private readonly logger = new Logger(AuthService.name);

  async login(user: User): Promise<Record<string, any>> {
    // Validation Flag
    let isOk = false;

    // Transform body into DTO
    const userDTO = new UsersDTO();
    userDTO.email = user.email;
    userDTO.password = user.password;

    // Validate DTO against validate function from class-validator
    await validate(userDTO).then((errors) => {
      if (errors.length > 0) {
        this.logger.debug(`${errors}`, AuthService.name);
      } else {
        isOk = true;
      }
    });

    if (isOk) {
      // Get user information
      const userDetails = await this.usersRepository.findOne({
        where: {
          email: user.email,
        },
      });
      if (userDetails == null) {
        return { status: 401, msg: { msg: 'Invalid credentials' } };
      }

      // Check if the given password match with saved password
      const isValid = bcrypt.compareSync(user.password, userDetails.password);
      if (isValid) {
        return {
          status: 200,
          msg: {
            email: user.email,
            access_token: this.jwtService.sign({ email: user.email }),
          },
        };
      } else {
        return { status: 401, msg: { msg: 'Invalid credentials' } };
      }
    } else {
      return { status: 400, msg: { msg: 'Invalid fields.' } };
    }
  }

  async createUser(body: any): Promise<Record<string, any>> {
    // Validation Flag
    let isOk = false;

    // Transform body into DTO
    const userDTO = new UsersDTO();
    userDTO.email = body.email;
    userDTO.password = bcrypt.hashSync(body.password, 10);

    // Validate DTO against validate function from class-validator
    await validate(userDTO).then((errors) => {
      if (errors.length > 0) {
        this.logger.debug(`${errors}`, AuthService.name);
      } else {
        isOk = true;
      }
    });
    if (isOk) {
      await this.usersRepository.save(userDTO).catch((error) => {
        this.logger.debug(error.message, AuthService.name);
        isOk = false;
      });
      if (isOk) {
        return { status: 201, content: { msg: `User created with success` } };
      } else {
        return { status: 400, content: { msg: 'User already exists' } };
      }
    } else {
      return { status: 400, content: { msg: 'Invalid content' } };
    }
  }

  async getUsers(): Promise<Record<string, any>> {
    let isOk = true;
    const users = await this.usersRepository.find().catch((error) => {
      this.logger.debug(`${error.message}`, AuthService.name);
      isOk = false;
    });
    if (isOk) {
      return { status: 200, content: { msg: users } };
    } else {
      return {
        status: 400,
        content: { msg: 'There was an error retrieving the list of users' },
      };
    }
  }
}
