import {
  Body,
  Controller,
  Post,
  Get,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './strategy/jwt-auth.guard';
import { UserBody } from './swagger';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async login(@Req() req, @Res() res, @Body() body: UserBody) {
    const auth = await this.authService.login(body);
    res.status(auth.status).json(auth.msg);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('user')
  async createUser(@Req() req, @Res() res, @Body() body: UserBody) {
    const auth = await this.authService.createUser(body);
    res.status(auth.status).json(auth.content);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('users')
  async getUsers(@Req() req, @Res() res) {
    const auth = await this.authService.getUsers();
    res.status(auth.status).json(auth.content);
  }
}
